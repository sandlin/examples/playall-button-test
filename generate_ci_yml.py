#!/usr/bin/env python
import sys
import argparse
import os
import jinja2
import configparser
from dataclasses import dataclass


def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Generate dynamic yml")
    parser.add_argument('--outputfile', type=str, help='The name of the file to generate', default='downstream_pipeline.yml')
    pargs = parser.parse_args(args)
    return pargs

def main(**kwargs):
    '''
    Generate the source object list then pass it to the jinja2 template to render the pipeline.
    :param argv: n/a
    :return: n/a
    '''
    outputfile = kwargs.get('outputfile')
    rendered = jinja2.Template(open('./templates/artifact_gitlab_ci.yml.j2').read()).render()
    fp = open(outputfile, 'w')
    n = fp.write(rendered)
    fp.close()

if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
